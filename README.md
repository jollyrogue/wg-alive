# wg-alive

Small program written in lua to check if a wireguard interface has a stale handshake and restart the interface if needed.