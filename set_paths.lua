-- set_paths.lua

local version = _VERSION:match("%d+%.%d+")
package.path = 'deps/share/lua/' .. version .. '/?.lua;deps/share/lua/' .. version .. '/?/init.lua;' .. package.path
package.cpath = 'deps/lib/lua/' .. version .. '/?.so;' .. package.cpath
