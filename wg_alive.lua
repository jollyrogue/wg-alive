#!/usr/bin/env lua51

local argparse = require("argparse")
local parser = argparse("wg_alive",
                        "Script to check if wireguard interfaces need to be restarted.")
parser:argument("ifname",
                "Name of wireguard interface to check.")
parser:option("-t, --max-time",
              "Maximum time, in seconds, between handshakes. (60s default)",
              60)
parser:option("-p, --ping-count",
              "Number of pings to send to the interface. (3 default)",
              3)
parser:option("-i, --ping-interval",
              "Seconds between each ping. (1s default)",
              1)
parser:option("-w, --wg-tool",
              "Path to a program which will give the status of the interface. (/usr/bin/wg default)",
              "/usr/bin/wg")

local args = parser:parse()

for k,v in pairs(args) do
  print(k .. ": \t" .. v .. " (" .. type(v) .. ")")
end

print()
local os_release = io.popen("cat -s /etc/os-release")
print(os_release:read("*a"))
